#include "io.h"

#include <stdio.h>
#include <stdlib.h>

int32_t io_read_move() {
  char* line = NULL;
  size_t line_length = 0;
  if (getline(&line, &line_length, stdin) == -1) {
    return -1;
  }

  if (ferror(stdin)) {
    return -1;
  }

  int32_t i, j;
  int32_t r = sscanf(line, "%d%d", &i, &j);
  free(line);
  if (r != 2) {
    return -1;
  }
  return 3 * i + j - 4;
}

void io_display_move(int32_t move) {
  printf("(%d, %d)\n", 1 + move / 3, 1 + move % 3);
}
