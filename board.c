#include "board.h"
#include "random.h"

#include <stdio.h>
#include <string.h>

#define N 3
#define NXN 9

struct board {
  enum board_player player;
  int32_t empties;
  char fields[NXN];
};

struct move_iterator {
  const char* fields;
  int32_t move;
};

static void board_next_player(struct board* self) {
  if (self->player == BOARD_PLAYER_X) {
    self->player = BOARD_PLAYER_O;
  } else {
    self->player = BOARD_PLAYER_X;
  }
}

static struct board instance, instance_copy;
struct board* board_create() {
  struct board* result = &instance;
  result->player = BOARD_PLAYER_X;
  result->empties = NXN;
  for (int32_t i = 0; i < NXN; ++i) {
    result->fields[i] = BOARD_PLAYER_NONE;
  }
  return result;
}

struct board* board_copy(const struct board* self) {
  memcpy(&instance_copy, self, sizeof(struct board));
  return &instance_copy;
}

bool board_validate_move(const struct board* self, int32_t move) {
  return move >= 0 && move < NXN && self->fields[move] == BOARD_PLAYER_NONE;
}

void board_do_move(struct board* self, int32_t move) {
  self->fields[move] = self->player;
  self->empties--;
  board_next_player(self);
}

void board_undo_move(struct board* self, int32_t move) {
  self->fields[move] = BOARD_PLAYER_NONE;
  self->empties++;
  board_next_player(self);
}

bool board_end(const struct board* self, enum board_player* winner) {
  *winner = BOARD_PLAYER_NONE;
  const char* s = self->fields;
  // check lines
  for (int32_t i = 0; i < NXN; i += 3) {
    if (s[i] != BOARD_PLAYER_NONE && s[i] == s[i + 1] && s[i + 1] == s[i + 2]) {
      *winner = (enum board_player)s[i];
      return true;
    }
  }
  // check columns
  for (int32_t i = 0; i < N; ++i) {
    if (s[i] != BOARD_PLAYER_NONE && s[i] == s[i + 3] && s[i + 3] == s[i + 6]) {
      *winner = (enum board_player)s[i];
      return true;
    }
  }
  // check first diagonal
  if (s[0] != BOARD_PLAYER_NONE && s[4] == s[0] && s[8] == s[4]) {
    *winner = (enum board_player)s[0];
    return true;
  }
  // check second diagonal
  if (s[2] != BOARD_PLAYER_NONE && s[4] == s[2] && s[6] == s[4]) {
    *winner = (enum board_player)s[2];
    return true;
  }

  return self->empties == 0;
}

enum board_player board_turn(const struct board* self) { return self->player; }

enum board_player board_opponent(const struct board* self) {
  return self->player == BOARD_PLAYER_X ? BOARD_PLAYER_O : BOARD_PLAYER_X;
}

void board_display(const struct board* self) {
  printf("\n\t1\t2\t3\n");
  for (int32_t i = 0; i < NXN; ++i) {
    if (i % N == 0) {
      printf("\n\n%d", 1 + i / N);
    }

    printf("\t%c", self->fields[i]);
  }
  printf("\n\n\n");
}

static struct move_iterator* move_iterator_create(const struct board* board) {
  static struct move_iterator iterator;
  iterator.fields = board->fields;
  iterator.move = -1;
  return &iterator;
}

static void move_iterator_advance(struct move_iterator* self) {
  int32_t* m = &self->move;
  for (++*m; *m != NXN && self->fields[*m] != BOARD_PLAYER_NONE; ++*m) {
  }
}

struct move_iterator* board_move_iterator(const struct board* self) {
  struct move_iterator* iterator = move_iterator_create(self);
  move_iterator_advance(iterator);
  return iterator;
}

bool move_iterator_valid(const struct move_iterator* self) {
  return (self->move != NXN);
}

void move_iterator_next(struct move_iterator* self) {
  if (self->move == NXN) {
    return;
  }
  move_iterator_advance(self);
}

int32_t move_iterator_value(const struct move_iterator* self) {
  return self->move;
}
