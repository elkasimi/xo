#ifndef __node_h__
#define __node_h__

#include <stdbool.h>
#include <stdint.h>

#include "board.h"

struct node;

void node_precompute();
void node_clear();
struct node* node_create(struct node* parent, const struct board* board,
                         int32_t move);
struct node* node_expand(struct node* self, struct board* board);
void node_update(struct node* self, double value);
struct node* node_select(const struct node* self);
struct node* node_select_most_visited(const struct node* self);
double node_value(const struct node* self);
int32_t node_visits(const struct node* self);
int32_t node_move(const struct node* self);
struct node* node_parent(const struct node* self);
bool node_leaf(const struct node* self);
bool node_fully_expanded(const struct node* self);

#endif
