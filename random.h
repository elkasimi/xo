#ifndef __random_h__
#define __random_h__

#include <stdint.h>

void random_randomize();
double random_real();
int32_t random_between(int32_t lower, int32_t upper);

#endif
