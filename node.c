#include "node.h"
#include "board.h"
#include "random.h"
#include "timer.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_NODES 1000001

struct node {
  struct node* parent;
  enum board_player player;
  int32_t move;
  double value;
  int32_t visits;
  bool leaf;
  int32_t untried_moves_count;
  int32_t untried_moves[9];
  struct node* next;
  struct node* first_child;
};

static int32_t node_index = 0;
static struct node nodes[MAX_NODES];

static void node_remove_untried_move(struct node* self, int32_t move_index) {
  self->untried_moves_count--;
  self->untried_moves[move_index] =
      self->untried_moves[self->untried_moves_count];
}

static void node_add_untried_move(struct node* self, int32_t move) {
  self->untried_moves[self->untried_moves_count] = move;
  self->untried_moves_count++;
}

void node_clear() { node_index = 0; }

struct node* node_create(struct node* parent, const struct board* board,
                         int32_t move) {
  if (node_index == MAX_NODES) {
    // TODO assert
    return NULL;
  }

  struct node* node = &nodes[node_index++];
  node->parent = parent;
  node->player = board_opponent(board);
  node->move = move;
  node->value = 0;
  node->visits = 0;
  node->leaf = true;
  node->untried_moves_count = 0;
  enum board_player winner;
  if (!board_end(board, &winner)) {
    for (struct move_iterator* iterator = board_move_iterator(board);
         move_iterator_valid(iterator); move_iterator_next(iterator)) {
      node->leaf = false;
      node_add_untried_move(node, move_iterator_value(iterator));
    }
  }
  node->next = NULL;
  node->first_child = NULL;
  return node;
}

struct node* node_expand(struct node* self, struct board* board) {
  int32_t i = random_between(0, self->untried_moves_count);
  int32_t move = self->untried_moves[i];
  node_remove_untried_move(self, i);
  board_do_move(board, move);
  struct node* n = node_create(self, board, move);
  n->next = self->first_child;
  self->first_child = n;
  return n;
}

void node_update(struct node* self, double value) {
  if (self->player == BOARD_PLAYER_O) {
    value = 1.0 - value;
  }
  self->value = (self->value * self->visits + value) / (self->visits + 1);
  self->visits++;
}

static double stored_sqrt_log[MAX_NODES], stored_sqrt[MAX_NODES];
void node_precompute() {
  time_point start, stop;
  timer_start(&start);
  for (int32_t i = 1; i < MAX_NODES; ++i) {
    stored_sqrt_log[i] = sqrt(log((double)i));
    stored_sqrt[i] = sqrt((double)i);
  }
  timer_stop(&stop);
  printf("precompute took %.3f\n", timer_get_delta_time(&start, &stop));
}

static double node_uct_value(const struct node* self,
                             const struct node* child) {
  const double k = 1.0;
  return child->value +
         k * stored_sqrt_log[self->visits] / stored_sqrt[child->visits];
}

static void visits_accumulator(const struct node* node, struct node* child,
                               struct node** best_child, double* max_visits) {
  (void)node;
  if (*max_visits < child->visits) {
    *max_visits = child->visits;
    *best_child = child;
  }
}

static void uct_value_accumulator(const struct node* node, struct node* child,
                                  struct node** best_child,
                                  double* max_uct_value) {
  double child_uct_value = node_uct_value(node, child);
  if (*max_uct_value < child_uct_value) {
    *max_uct_value = child_uct_value;
    *best_child = child;
  }
}

typedef void (*accumulator_function)(const struct node*, struct node*,
                                     struct node**, double*);

static struct node* node_accumulate(const struct node* self,
                                    accumulator_function accumulator) {
  double best_value = -1.0;
  struct node* best_child = NULL;
  for (struct node* child = self->first_child; child != NULL;
       child = child->next) {
    accumulator(self, child, &best_child, &best_value);
  }
  return best_child;
}

struct node* node_select(const struct node* self) {
  return node_accumulate(self, uct_value_accumulator);
}

struct node* node_select_most_visited(const struct node* self) {
  return node_accumulate(self, visits_accumulator);
}

double node_value(const struct node* self) { return self->value; }

int32_t node_visits(const struct node* self) { return self->visits; }

int32_t node_move(const struct node* self) { return self->move; }

struct node* node_parent(const struct node* self) {
  return self->parent;
}

bool node_leaf(const struct node* self) { return self->leaf; }

bool node_fully_expanded(const struct node* self) {
  return self->untried_moves_count == 0;
}
