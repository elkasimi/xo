#include "random.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

void random_randomize() { srand(time(NULL)); }

double random_real() { return (double)rand() / RAND_MAX; }

int32_t random_between(int32_t lower, int32_t upper) {
  double x = (upper - lower) * random_real();
  return lower + ((int32_t)floor(x));
}
