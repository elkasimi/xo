#include "board.h"
#include "io.h"
#include "mcts.h"
#include "node.h"
#include "random.h"

#include <stdio.h>

static int32_t read_user_move(const struct board* board) {
  int32_t move;
  for (move = io_read_move(); !board_validate_move(board, move);
       move = io_read_move()) {
    printf(
        "Invalid move!\n"
        "Please enter line and column of your move like:\n"
        "line column (for example 1 3)\n");
  }
  return move;
}

enum board_player main_loop(enum board_player ai) {
  random_randomize();
  node_precompute();
  struct board* board = board_create();
  board_display(board);
  enum board_player winner;
  for (; !board_end(board, &winner);) {
    int32_t move;
    if (board_turn(board) == ai) {
      move = mcts_get_best_move(board);
    } else {
      move = read_user_move(board);
    }
    board_do_move(board, move);
    board_display(board);
  }
  return winner;
}

int32_t main(int32_t argc, char* argv[]) {
  enum board_player ai = BOARD_PLAYER_X;
  printf("Bot playing as ");
  if (argc > 1 && argv[1][0] == 'o') {
    ai = BOARD_PLAYER_O;
    printf("O ..\n");
  } else {
    printf("X ..\n");
  }
  enum board_player winner = main_loop(ai);
  if (winner == BOARD_PLAYER_NONE) {
    printf("Draw\n");
  } else {
    printf("%c wins\n", winner);
  }

  return 0;
}
