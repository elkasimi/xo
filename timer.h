#ifndef __timer_h__
#define __timer_h__

#include "time_point.h"

void timer_start(time_point* start);
void timer_stop(time_point* stop);
double timer_get_delta_time(const time_point* start, const time_point* stop);

#endif
