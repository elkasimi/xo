#ifndef __io_h__
#define __io_h__

#include <stdint.h>

int32_t io_read_move();
void io_display_move(int32_t move);

#endif
