#ifndef __board_h__
#define __board_h__

#include <stdbool.h>
#include <stdint.h>

struct board;
struct move_iterator;

enum board_player {
  BOARD_PLAYER_X = 'X',
  BOARD_PLAYER_O = 'O',
  BOARD_PLAYER_NONE = '.'
};

struct board* board_create();
struct board* board_copy(const struct board* self);
bool board_validate_move(const struct board* self, int32_t move);
void board_do_move(struct board* self, int32_t move);
void board_undo_move(struct board* self, int32_t move);
bool board_end(const struct board* self, enum board_player* winner);
enum board_player board_turn(const struct board* self);
enum board_player board_opponent(const struct board* self);
void board_display(const struct board* self);
struct move_iterator* board_move_iterator(const struct board* self);
bool move_iterator_valid(const struct move_iterator* self);
void move_iterator_next(struct move_iterator* self);
int32_t move_iterator_value(const struct move_iterator* self);

#endif
