#ifndef __mcts_h__
#define __mcts_h__

#include <stdint.h>
#include "board.h"

int32_t mcts_get_best_move(const struct board* board);

#endif
