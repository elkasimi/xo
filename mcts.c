#include "mcts.h"
#include "node.h"
#include "random.h"
#include "timer.h"

#include <stdio.h>
#include <stdlib.h>

#define MAX_SIMULATIONS 100000

static int32_t mcts_get_default_policy_move(const struct board* board) {
  int32_t count = 0, possible[9];
  for (struct move_iterator* iterator = board_move_iterator(board);
       move_iterator_valid(iterator); move_iterator_next(iterator)) {
    possible[count++] = move_iterator_value(iterator);
  }
  return possible[random_between(0, count)];
}

static double mcts_run_simulation(struct board* board) {
  enum board_player winner;
  while (!board_end(board, &winner)) {
    board_do_move(board, mcts_get_default_policy_move(board));
  }
  switch (winner) {
    case BOARD_PLAYER_X:
      return 1.0;
    case BOARD_PLAYER_O:
      return 0.0;
    default:
      return 0.5;
  }
}

int32_t mcts_get_best_move(const struct board* board) {
  time_point start, stop;
  timer_start(&start);
  struct node* root = node_create(NULL, board, -1);
  int32_t i;
  for (i = 0; i < MAX_SIMULATIONS; ++i) {
    struct board* b = board_copy(board);
    struct node* node = root;
    for (; node_fully_expanded(node) && !node_leaf(node);) {
      node = node_select(node);
      board_do_move(b, node_move(node));
    }

    if (!node_fully_expanded(node)) {
      node = node_expand(node, b);
    }

    double value = mcts_run_simulation(b);
    for (; node != NULL; node = node_parent(node)) {
      node_update(node, value);
    }
  }

  struct node* most_visited = node_select_most_visited(root);
  timer_stop(&stop);
  printf("==> i=%d w=%.2f%% v=%d dt=%.3f\n", i, 100 * node_value(most_visited),
         node_visits(most_visited), timer_get_delta_time(&start, &stop));
  printf("Expected");
  for (struct node* node = node_select_most_visited(root);
       node_fully_expanded(node) && !node_leaf(node);) {
    printf("=>(%d,%d)", 1 + node_move(node) / 3, 1 + node_move(node) % 3);
    node = node_select_most_visited(node);
  }
  printf("\n");
  int32_t best_move = node_move(most_visited);
  node_clear();
  return best_move;
}
