#include "timer.h"

#include <stdlib.h>

void timer_start(time_point* start) { gettimeofday(start, NULL); }

void timer_stop(time_point* stop) { gettimeofday(stop, NULL); }

static double time_point_to_double(const time_point* tv) {
  return 1.0 * tv->tv_sec + 1e-6 * tv->tv_usec;
}

double timer_get_delta_time(const time_point* start, const time_point* stop) {
  return time_point_to_double(stop) - time_point_to_double(start);
}
